//
//  constants.swift
//  RainyShiny
//
//  Created by Maciej Chmielewski on 29.03.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import Foundation

var CURRENT_WEATHER_URL: String {
  if let latitude = Location.sharedInstance.latitude, let longitude = Location.sharedInstance.longitude {
    return "http://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&appid=38857772d9ec173e72c298cea9b3f37c"
  } else {
    //If current location is not available return URL for LA
    return "http://api.openweathermap.org/data/2.5/weather?lat=34.052235&lon=-118.243683&appid=38857772d9ec173e72c298cea9b3f37c"
  }
}

var FORECAST_URL: String {
  if let latitude = Location.sharedInstance.latitude, let longitude = Location.sharedInstance.longitude {
    return "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(latitude)&lon=\(longitude)&cnt=10&appid=38857772d9ec173e72c298cea9b3f37c"
  } else {
    //If current location is not available return URL for LA
    return "http://api.openweathermap.org/data/2.5/forecast/daily?lat=34.052235&lon=-118.243683&cnt=10&appid=38857772d9ec173e72c298cea9b3f37c"
  }
}

typealias DownloadComplete = () -> ()

extension Date {
  var dayOfTheWeek: String {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "EEEE"
      return dateFormatter.string(from: self)
  }
}


