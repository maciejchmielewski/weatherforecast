//
//  WeatherVC.swift
//  RainyShiny
//
//  Created by Maciej Chmielewski on 28.03.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class WeatherVC: UIViewController, UITableViewDelegate, UITableViewDataSource , CLLocationManagerDelegate{
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var currentTempLabel: UILabel!
  @IBOutlet weak var currentLocationLabel: UILabel!
  @IBOutlet weak var currentWeatherImage: UIImageView!
  @IBOutlet weak var currentWeatherLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  
  private var locationManager: CLLocationManager!
  private var location: CLLocation!
  private var currentWeather: CurrentWeather!
  private var forecast: Forecast!
  private var forecasts = [Forecast]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    locationManager = CLLocationManager()
    locationManager.requestWhenInUseAuthorization()
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    tableView.delegate = self
    tableView.dataSource = self
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    manager.startMonitoringSignificantLocationChanges()
    if let location = manager.location {
      Location.sharedInstance.latitude = location.coordinate.latitude
      Location.sharedInstance.longitude = location.coordinate.longitude
      self.location = location
      updateData()
    }
  }
  
  private func updateData() {
    currentWeather = CurrentWeather()
    currentWeather.downloadWeatherDetails {
      self.downloadForecastData {
        self.updateVC()
      }
    }
  }
  
  private func downloadForecastData(completed: @escaping DownloadComplete)  {
    guard let forecastUrl = URL(string: FORECAST_URL) else { return }
    Alamofire.request(forecastUrl).responseJSON { response in
      let response = response.result
      if let dict = response.value as? [String: AnyObject] {
        if let list = dict["list"] as? [[String: AnyObject]] {
          for obj in list {
            let forecast = Forecast(weatherDict: obj)
            self.forecasts.append(forecast)
          }
        }
      }
      completed()
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? WeatherCell {
      let forecast = forecasts[indexPath.row]
      cell.configureCell(forecast: forecast)
      return cell
    } else {
      return WeatherCell()
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return forecasts.count
  }
  
  private func updateVC() {
    tableView.reloadData()
    dateLabel.text = currentWeather.date
    currentLocationLabel.text = currentWeather.cityName
    currentTempLabel.text = "\(currentWeather.currentTemp)"
    self.currentWeatherLabel.text = currentWeather.weatherType
    self.currentWeatherImage.image = UIImage(named: currentWeather.weatherType)
  }
}

