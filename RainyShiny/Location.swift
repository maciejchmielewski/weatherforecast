//
//  Location.swift
//  RainyShiny
//
//  Created by Maciej Chmielewski on 30.03.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import CoreLocation

class Location {
  
  static var sharedInstance = Location()
  
  var longitude: Double!
  var latitude: Double!
}

