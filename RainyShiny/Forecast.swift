//
//  Forecast.swift
//  RainyShiny
//
//  Created by Maciej Chmielewski on 30.03.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit
import Alamofire

class Forecast {
  
  private var _date: String!
  private var _weatherType: String!
  private var _highTemp: Int!
  private var _lowTemp: Int!
  
  var date: String{
    if _date == nil{
      _date = ""
    }
    return _date
  }
  
  var weatherType: String{
    if _weatherType == nil{
      _weatherType = ""
    }
    return _weatherType
  }
  
  var highTemp: Int{
    if _highTemp == nil{
      _highTemp = 0
    }
    return _highTemp
  }
  
  var lowTemp: Int{
    if _lowTemp == nil{
      _lowTemp = 0
    }
    return _lowTemp
  }
  
  init(weatherDict: [String: AnyObject]) {
    if let temp = weatherDict["temp"] as? [String: AnyObject] {
      if let min = temp["min"] as? Int {
        let kelvinTemp = min
        let celsiusTemp = kelvinTemp - 273
        self._lowTemp = celsiusTemp
      }
      if let max = temp["max"] as? Int {
        let kelvinTemp = max
        let celsiusTemp = kelvinTemp - 273
        self._highTemp = celsiusTemp
      }
    }
    if let weather = weatherDict["weather"] as? [[String: AnyObject]] {
      if let main = weather[0]["main"] as? String {
        self._weatherType = main
      }
    }
    if let date = weatherDict["dt"] as? Double {
      let unixConvertedDate = Date(timeIntervalSince1970: date)
      self._date = unixConvertedDate.dayOfTheWeek
    }
  }
}
