//
//  CurrentWeather.swift
//  RainyShiny
//
//  Created by Maciej Chmielewski on 29.03.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
  
  //Private API
  private var _cityName: String!
  private var _weatherType: String!
  private var _date: String!
  private var _currentTemp: Int!
  
  //Public API
  var cityName: String {
    if _cityName == nil {
      _cityName = ""
    }
    return _cityName
  }
  
  var weatherType: String {
    if _weatherType == nil {
      _weatherType = ""
    }
    return _weatherType
  }
  
  var date: String {
    if _date == nil {
      _date = ""
    }
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .long
    dateFormatter.timeStyle = .none
    let dateString = dateFormatter.string(from: Date())
    _date = "Today, \(dateString)"
    return _date
  }
  
  var currentTemp: Int {
    if _currentTemp == nil {
      _currentTemp = 0
    }
    return _currentTemp
  }
  
  func downloadWeatherDetails(completed: @escaping DownloadComplete) {
    guard let weatherUrl = URL(string: CURRENT_WEATHER_URL) else { return }
    Alamofire.request(weatherUrl).responseJSON { response in
      let result = response.result
      if let dict = result.value as? [String: AnyObject] {
        if let name = dict["name"] as? String {
          self._cityName = name
        }
        if let weather = dict["weather"] as? [[String: AnyObject]] {
          if let main = weather[0]["main"] as? String {
            self._weatherType = main
          }
        }
        if let main = dict["main"] as? [String: AnyObject] {
          if let kelvinTemp = main["temp"] as? Int {
            let celsiusTemp = kelvinTemp - 273
            self._currentTemp = celsiusTemp
          }
        }
      }
      completed()
    }
  }
}
